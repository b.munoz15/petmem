# API de Mascotas

¡Bienvenido a la API de Mascotas! Esta API te permite realizar operaciones CRUD (Crear, Leer, Actualizar, Eliminar) relacionadas con mascotas.

## Tabla de Contenidos

- [Requisitos](#requisitos)
- [Configuración](#configuración)
- [Rutas](#rutas)
- [Ejemplos de Solicitudes](#ejemplos-de-solicitudes)
- [Respuestas](#respuestas)

## Requisitos

Asegúrate de tener instalado lo siguiente:

- Node.js
- PostgreSQL (u otro sistema de gestión de bases de datos)

## Configuración

1. Clona este repositorio o descarga los archivos de la API.

2. Ejecuta `npm install` para instalar las dependencias.

3. Configura la conexión a la base de datos en `config/database.js`.

4. Ejecuta la aplicación con `npm start`.

## Rutas

- `POST /mascotas`: Crea una nueva mascota.
- `GET /mascotas`: Obtiene todas las mascotas.
- `GET /mascotas/:id`: Obtiene una mascota por su ID.
- `PUT /mascotas/:id`: Actualiza una mascota por su ID.
- `DELETE /mascotas/:id`: Elimina una mascota por su ID.

## Ejemplos de Solicitudes

### Crear una nueva mascota

POST http://localhost:3000/mascotas

{
  "name": "Firulais",
  "age": 3,
  "size": "Mediano",
  "breed": "Labrador",
  "gender": "Macho",
  "description": "Un perro amigable y juguetón."
}

### Obtener todas las mascotas

GET http://localhost:3000/mascotas

### Obtener mascota por id

GET http://localhost:3000/mascotas/1

### Actualizar mascota por id

PUT http://localhost:3000/mascotas/1

{
  "age": 4,
  "description": "Un perro amigable y activo."
}

### Eliminar una mascota por su ID

DELETE http://localhost:3000/mascotas/1

## Respuestas
La API responderá con datos en formato JSON.
Las respuestas exitosas incluirán un código de estado HTTP 200 o 201.
Las respuestas exitosas sin contenido usarán el código de estado HTTP 204.
Las respuestas de error incluirán un código de estado HTTP 4xx o 5xx.




