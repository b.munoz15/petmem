var express = require('express');
var router = express.Router();

//Importa el petcontroller.js
var petController = require('../src/controller/petController'); 


// Rutas relacionadas con las mascotas
router.use('/mascotas', petController);

module.exports = router;
