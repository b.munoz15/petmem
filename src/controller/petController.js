const express = require('express');
const petService = require('../service/petService'); // Importa el servicio de mascotas

const router = express.Router();

// Ruta para crear una nueva mascota
router.post('/', async (req, res) => {
  try {
    const nuevaMascota = await petService.createPet(req.body);
    res.status(201).json(nuevaMascota);
  } catch (error) {
    console.error('Error al crear una mascota:', error);
    res.status(500).json({ error: 'Error al crear una mascota' });
  }
});

// Ruta para obtener todas las mascotas
router.get('/', async (req, res) => {
  try {
    const mascotas = await petService.getAllPets();
    res.json(mascotas);
  } catch (error) {
    console.error('Error al buscar mascotas:', error);
    res.status(500).json({ error: 'Error al buscar mascotas' });
  }
});

// Ruta para obtener una mascota por su ID
router.get('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const mascota = await petService.getPetById(id);
    if (mascota) {
      res.json(mascota);
    } else {
      res.status(404).json({ message: 'Mascota no encontrada' });
    }
  } catch (error) {
    console.error('Error al buscar una mascota por ID:', error);
    res.status(500).json({ error: 'Error al buscar una mascota' });
  }
});

// Ruta para actualizar una mascota por su ID
router.put('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const datosActualizados = req.body;
    const mascotaActualizada = await petService.updatePet(id, datosActualizados);
    if (mascotaActualizada) {
      res.json(mascotaActualizada);
    } else {
      res.status(404).json({ message: 'Mascota no encontrada' });
    }
  } catch (error) {
    console.error('Error al actualizar una mascota:', error);
    res.status(500).json({ error: 'Error al actualizar una mascota' });
  }
});

// Ruta para eliminar una mascota por su ID
router.delete('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const resultado = await petService.deletePet(id);
    if (resultado) {
      res.status(204).send(); // Respuesta exitosa sin contenido
    } else {
      res.status(404).json({ message: 'Mascota no encontrada' });
    }
  } catch (error) {
    console.error('Error al eliminar una mascota:', error);
    res.status(500).json({ error: 'Error al eliminar una mascota' });
  }
});

module.exports = router;
