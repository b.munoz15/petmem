const Sequelize = require('sequelize');
const sequelize = require('../config/sequelize'); // Importa la configuración de Sequelize

const Pet = sequelize.define('Pet', {
    id_pet: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: Sequelize.STRING,
    age: Sequelize.INTEGER,
    size: Sequelize.STRING,
    breed: Sequelize.STRING,
    gender: Sequelize.STRING,
    description: Sequelize.STRING,
  });
  
  module.exports = Pet;
