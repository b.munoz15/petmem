const Pet = require('../model/petModel'); // Importa el modelo "Pet" definido en PetModel.js

// Crear una nueva mascota
async function createPet(data) {
  try {
    const nuevaMascota = await Pet.create(data);
    return nuevaMascota;
  } catch (error) {
    throw error;
  }
}

// Obtener todas las mascotas
async function getAllPets() {
  try {
    const mascotas = await Pet.findAll();
    return mascotas;
  } catch (error) {
    throw error;
  }
}

// Obtener una mascota por su ID
async function getPetById(id) {
  try {
    const mascota = await Pet.findByPk(id);
    return mascota;
  } catch (error) {
    throw error;
  }
}

// Actualizar una mascota por su ID
async function updatePet(id, data) {
  try {
    const [rowsUpdated, [mascotaActualizada]] = await Pet.update(data, {
      where: { id_pet: id },
      returning: true,
    });
    return mascotaActualizada;
  } catch (error) {
    throw error;
  }
}

// Eliminar una mascota por su ID
async function deletePet(id) {
  try {
    const resultado = await Pet.destroy({
      where: { id_pet: id },
    });
    return resultado;
  } catch (error) {
    throw error;
  }
}

module.exports = {
  createPet,
  getAllPets,
  getPetById,
  updatePet,
  deletePet,
};
